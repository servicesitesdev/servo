<?php

//Conditional Brand-Specific Content
//$url = 'https://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
//
//if (strpos($url, 'ford') !== false || strpos($url, 'lincoln') !== false) {
//
//
//}
//elseif (strpos($url, 'localhost') !== false) {
//
//    function show_template() {
//        global $template;
//        print_r( $template );
//    }
//    echo '<div style="background-color:#c6e5d9;color: #333;text-align:center;"><b>Development Mode</b></div>';
//    show_template();
//}
?>

<script>
    var partsLinkShortcode = '<?= do_shortcode('[parts-link]') ?>';
    var accLinkShortcode = '<?= do_shortcode('[accessories-link]') ?>';
    var tiresLinkShortcode = '<?= do_shortcode('[tires-link]') ?>';
    var partsPopupModal = "popupModal(partsLinkShortcode, '', '1000', '800')";
    var accPopupModal = "popupModal(accLinkShortcode, '', '1000', '800')";
    var tiresPopupModal = "popupModal(tiresLinkShortcode, '', '1000', '800')";
    var carFoxPopupModal = "popupModal('https://www.mycarfax.com/', '', '1000', '800')";
    var mainSiteCta = '<ul style="background-color:#c51a47!important; display: block;" class="nav navbar-nav navbar-right hidden-xs"><li><a class="mainsitecta visible-lg" target="_blank" href="<?= do_shortcode('[mainsite-link]') ?>">Main Site <i class="fa fa-fw fa-car margin-left_5x"></i></a></li></ul>';

    $("ul#menu-main-nav").after(mainSiteCta);
    if ($(window).width() < 1130) {
        $("li.nav_parts>a").prepend('<i style="padding-right:3px!important;vertical-align:middle;text-decoration:none;display:inline-block;" class="fa fa-fw fa-cogs"></i>');
        $("li.nav_parts>a").attr({'target': '_blank', 'href': partsLinkShortcode});
        $("li.nav_acc>a").attr({'target': '_blank', 'href': accLinkShortcode});
        $("li.nav_parts_footer>a").attr({'target': '_blank', 'href': partsLinkShortcode});
        $("li.nav_tire>a").attr({'target': '_blank', 'href': tiresLinkShortcode});
        $("li.nav_tire_footer>a").attr({'target': '_blank', 'href': tiresLinkShortcode});
        $("li.nav_carfox>a").attr({'target': '_blank', 'href': 'https://www.mycarfax.com/'});
    }

    else {
        $("li.nav_parts>a").attr({'style': 'outline:none!important;cursor:pointer','type': 'button','onclick': partsPopupModal});
        $("li.nav_acc>a").attr({'style': 'outline:none!important;cursor:pointer','type': 'button','onclick': accPopupModal});
        $("li.nav_parts>a").prepend('<i style="padding-right:3px!important;vertical-align:middle;text-decoration:none;display:inline-block;" class="fa fa-fw fa-cogs"></i>');
        $("li.nav_parts_footer>a").attr({'style': 'outline:none!important;cursor:pointer','type': 'button','onclick': partsPopupModal});
        $("li.nav_tire>a").attr({'style': 'outline:none!important;cursor:pointer','type': 'button','onclick': tiresPopupModal});
        $("li.nav_tire_footer>a").attr({'style': 'outline:none!important;cursor:pointer','type': 'button','onclick': tiresPopupModal});
        $("li.nav_carfox>a").attr({'style': 'outline:none!important;cursor:pointer','type': 'button', 'onclick': carFoxPopupModal});
    }
</script>