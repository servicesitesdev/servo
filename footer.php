<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package servo
 */
?>
<div id="map"></div>
<footer>
    <div class="bg-main-med">
        <div class="container">
            <div class="row">
                <div class="col-md-8 hidden-xs hidden-sm">
                    <div class="row">
                        <div class="col-sm-4">
                            <ul class="list-unstyled">
                                <?php
                                wp_nav_menu(array(
                                        'menu' => 'footer-nav1',
                                        'theme_location' => 'footer1',
                                        'depth' => 1,
                                        'container' => 'div',

                                        'menu_class' => 'footer navbar-nav1',
                                        'fallback_cb' => 'navwalker::fallback',
                                        'walker' => new navwalker()
                                    )
                                );
                                ?>
                            </ul>
                        </div>
                        <div class="col-sm-4">
                            <ul class="list-unstyled">
                                <?php
                                wp_nav_menu(array(
                                        'menu' => 'footer-nav2',
                                        'theme_location' => 'footer2',
                                        'depth' => 1,
                                        'container' => 'div',
                                        'container_class' => 'list-unstyled',
                                        'menu_class' => 'footer navbar-nav2',
                                        'fallback_cb' => 'navwalker::fallback',
                                        'walker' => new navwalker()
                                    )
                                );
                                ?>
                            </ul>
                        </div>
                        <div class="col-sm-4">
                            <ul class="list-unstyled">
                                <?php
                                wp_nav_menu(array(
                                        'menu' => 'footer-nav3',
                                        'theme_location' => 'footer3',
                                        'depth' => 1,
                                        'container' => 'div',
                                        'container_class' => 'list-unstyled',
                                        'menu_class' => 'footer navbar-nav3',
                                        'fallback_cb' => 'navwalker::fallback',
                                        'walker' => new navwalker()
                                    )
                                );
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-md-uncentered text-center text-left-md">
                    <p class="visible-xs"><a href="/schedule-service"
                                             class="btn btn-block btn-primary schedule-cta-test-01">
                            <i class="fa fa-fw fa-calendar"></i> Make Your Appointment</a> <a
                            href="tel:<?= do_shortcode('[tel-link]') ?>"
                            class="btn btn-block btn-white-outline"><i
                                class="fa fa-fw fa-mobile"></i> Call Us</a> <a href="/services/#express"
                                                                               class="btn btn-block btn-white-outline schedule-cta-test-01"><i
                                class="fa fa-fw fa-bolt"></i> Express Service</a></p>
                    <p class="hidden-xs visible-sm"><a href="/schedule-service" class="btn btn-primary"><i
                                class="fa fa-fw fa-calendar"></i> Make Your Appointment</a> <a
                            href="/services/#express"
                            class="btn btn-block btn-white-outline schedule-cta-test-01"><i
                                class="fa fa-fw fa-bolt"></i> Express Service</a></p>
                    <p class="hidden-xs hidden-sm"><a href="/schedule-service"
                                                      class="btn btn-block btn-primary schedule-cta-test-01"><i
                                class="fa fa-fw fa-calendar"></i> Make Your Appointment</a> <a
                            href="/services/#express"
                            class="btn btn-block btn-white-outline schedule-cta-test-01"><i
                                class="fa fa-fw fa-bolt"></i> Express Service</a></p>
<!--                    <div class="row">-->
<!--                        <div class="col-xs-6 col-sm-4 col-sm-offset-4 col-md-6 col-md-offset-0">-->
<!--                            <img id="aselogo" class="img-responsive" alt="ASE Certified"-->
<!--                                 src="--><?//= get_template_directory_uri(); ?><!--/assets/ase-cert-white.png"/>-->
<!--                        </div>-->
<!--                    </div>-->
                </div>
            </div>
        </div>
    </div>
    <div class="bg-main-drk">
        <div class="container text-center">
            <p class="margin-vert-1x">
                <small><a href="/" class="margin-right-1x"><i
                            class="fa fa-fw fa-copyright"></i> <?= date("Y"); ?> <?php bloginfo('name'); ?></a>
                    <span class="hidden-xs">|</span> <br class="visible-xs"><a
                        href="/sitemap/" class="margin-horz-1x"><i class="fa fa-fw fa-sitemap"></i> Sitemap</a> | <a
                        href="/privacy/" class="margin-left-1x"><i class="fa fa-user-secret" aria-hidden="true"></i>
                        Privacy</a></small>
            </p>
        </div>
    </div>
</footer>
<a href="javascript:void(0);" id="scroll" title="Scroll to Top" style="display: none;"><span></span></a>
<?php wp_footer(); ?>
<?= do_shortcode('[customcode-footer]') ?>
<?php include_once('footerContent.php'); ?>
<script>
    var map;
    function initMap() {

        var location = {lat: <?= do_shortcode('[lat-option]') ?>, lng: <?= do_shortcode('[lng-option]') ?>};

        var content = "<h4 class='margin-bottom-x pad-top-1x'><strong><?= do_shortcode('[dealer-name]') ?> Service Center</strong></h4><p><?= do_shortcode('[location-1]') ?>, <?= do_shortcode('[location-2]') ?> <br><i class='fa fa-mobile'></i> <?= do_shortcode('[service-tel]') ?></p><p><a class='btn btn-main' href='<?= do_shortcode('[location-link]') ?>' target='_blank'>Get Directions</a></p>";

        map = new google.maps.Map(document.getElementById('map'), {
            center: location,
            zoom: 15,
            styles: [
                {
                    featureType: 'all',
                    stylers: [
                        {saturation: -80}
                    ]
                }, {
                    featureType: 'road.arterial',
                    elementType: 'geometry',
                    stylers: [
                        {hue: '#00ffee'},
                        {saturation: 50}
                    ]
                }, {
                    featureType: 'poi.business',
                    elementType: 'labels',
                    stylers: [
                        {visibility: 'off'}
                    ]
                }
            ],
            scrollwheel: false
        });
        var marker = new google.maps.Marker({
            position: location,
            icon: "https://blueshirtcode.com/wp-content/themes/servo/assets/mapicon.png",
            title: "<?= do_shortcode('[dealer-name]') ?> Service Center",
            address: "",
            map: map,
        });
        google.maps.event.addListenerOnce(map, 'tilesloaded', function (event) {
            infowindow.open(map, marker);
        });
        var infowindow = new google.maps.InfoWindow({
            content: content
        });
    }
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB9Mlx8E-r63GkmdnxGwXHOVT8m9jqwjoY&callback=initMap"></script>
</body>
</html>
