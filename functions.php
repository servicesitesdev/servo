<?php
/**
 * servo functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package servo
 */

if (!function_exists('servo_setup')) :
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */
    function servo_setup()
    {
        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based on servo, use a find and replace
         * to change 'servo' to the name of your theme in all the template files.
         */
        load_theme_textdomain('servo', get_template_directory() . '/languages');

        // Add default posts and comments RSS feed links to head.
        add_theme_support('automatic-feed-links');

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support('title-tag');

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
         */
        add_theme_support('post-thumbnails');

        // This theme uses wp_nav_menu() in one location.
        //register_nav_menus( array(
        //'menu-1' => esc_html__( 'Primary', 'servo' ),
        //) );

        // Register Custom Navigation Walker
        require_once('navwalker.php');

        register_nav_menus(

            array(
                'primary' => esc_html__('Primary', 'servo'),
                'footer-nav1' => esc_html__('Footer Navigation 1', 'servo'),
                'footer-nav2' => esc_html__('Footer Navigation 2', 'servo'),
                'footer-nav3' => esc_html__('Footer Navigation 3', 'servo')
            )
        );

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support('html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ));

        // Set up the WordPress core custom background feature.
        add_theme_support('custom-background', apply_filters('servo_custom_background_args', array(
            'default-color' => 'ffffff',
            'default-image' => '',
        )));

        // Add theme support for selective refresh for widgets.
        add_theme_support('customize-selective-refresh-widgets');
    }
endif;
add_action('after_setup_theme', 'servo_setup');

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function servo_content_width()
{
    $GLOBALS['content_width'] = apply_filters('servo_content_width', 640);
}

add_action('after_setup_theme', 'servo_content_width', 0);

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function servo_widgets_init()
{
    register_sidebar(array(
        'name' => esc_html__('Sidebar', 'servo'),
        'id' => 'sidebar-1',
        'description' => esc_html__('Add widgets here.', 'servo'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>',
    ));
}

add_action('widgets_init', 'servo_widgets_init');

/**
 * Enqueue scripts and styles.
 */
function servo_scripts()
{
    wp_enqueue_style('servo-style', get_stylesheet_uri());
    wp_enqueue_script('servo-script', get_template_directory_uri() . '/main.js', array(), false, true);

    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }
}

add_action('wp_enqueue_scripts', 'servo_scripts');

//Admin Panel CSS
function admin_register_head()
{
    $url = get_bloginfo('template_directory') . '/dashboard/css/settings.css';
    echo "<link rel='stylesheet' type='text/css' href='$url' />\n";
}

add_action('admin_head', 'admin_register_head');

add_filter('wp_default_editor', create_function('', 'return "html";'));
//Remove Stupid Ass TinyMCE p tags
remove_filter('the_content', 'wpautop');
remove_filter('the_excerpt', 'wpautop');

add_filter('widget_text', 'shortcode_unautop');
add_filter('widget_text', 'do_shortcode');

require_once('serviceData.php');
require_once('dashboard/theme-options.php');
require_once('shortcodes/initialize.php');

/**
 * Enable ACF 5 early access
 * Requires at least version ACF 4.4.12 to work
 */
define('ACF_EARLY_ACCESS', '5');

// Set ACF 5 license key on theme activation. Stick in your functions.php or equivalent.
function auto_set_license_keys()
{

    if (!get_option('acf_pro_license') && defined('ACF_5_KEY')) {

        $save = array(
            'key' => ACF_5_KEY,
            'url' => home_url()
        );

        $save = maybe_serialize($save);
        $save = base64_encode($save);

        update_option('acf_pro_license', $save);
    }
}

add_action('after_switch_theme', 'auto_set_license_keys');


// ACF Options
if (function_exists('acf_add_options_page')) {

    acf_add_options_page(array(
        'page_title' => 'Coupon Generator',
        'menu_title' => 'Coupons',
        'menu_slug' => 'coupon-generator-settings',
        'capability' => 'edit_posts',
        'icon_url' => 'dashicons-tickets-alt',
        'position' => 18,
        'redirect' => false
    ));

    acf_add_options_page(array(
        'page_title' => 'Staff Settings',
        'menu_title' => 'Staff',
        'menu_slug' => 'staff-settings',
        'capability' => 'edit_posts',
        'icon_url' => 'dashicons-groups',
        'position' => 19,
        'redirect' => false
    ));

}

require_once 'acf-json/acf-initializer.php';
