<?php /* Template Name: Sitemap */

get_header(); ?>

<style>
    li.pagenav {
        font-size: 1.2em;
    }
</style>
<div id="sitemap">
    <section>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1>Sitemap</h1>
                    <hr>
                </div>
                <div class="col-md-8">
                    <div class="row">
                        <?php wp_list_pages() ?>
                    </div>
                    <hr class="visible-xs visible-sm">
                </div>
<!--                <div class="col-md-4">-->
<!--                    <h3 class="thin">Send us a Message</h3>-->
<!--                </div>-->
            </div>
        </div>
    </section>
</div>
<?php
//get_sidebar();
get_footer();
?>


