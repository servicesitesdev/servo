<?php

function serviceData()
{

    echo <<<dataContent
    <script>
    var _document = document,
        _dLocation = _document.location.href,
        _dPathname = _dLocation.pathname,
        _dSearchQuery = location.search,
        _dReferrer = _document.referrer;
    
    serviceData = {
        'event': [],
        'site': {
            'htmlContent': _document,
            'urlPath': _dLocation,
            'pathName': _dPathname,
            'urlSearch': _dSearchQuery,
            'referrer': _dReferrer
            }
    };
    </script>
dataContent;
}

add_action('wp_head', 'serviceData');