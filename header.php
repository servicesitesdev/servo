<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package servo
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="google-site-verification" content="<?= do_shortcode('[meta-google]') ?>"/>
    <meta name="msvalidate.01" content="F59F538D62AB4E31EF306B4C9774D33F"/>
    <?php wp_head(); ?>
    <!-- DYNAMIC METAINFO -->
    <?php include_once('metainfo.php'); ?>
    <!-- END DYNAMIC METAINFO -->
    <?= do_shortcode('[customcode-header]') ?>
    <!-- Google Tag Manager -->
    <script>(function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(), event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', '<?= do_shortcode('[analytics-gtmid]') ?>');</script>
    <!-- End Google Tag Manager -->
    <!-- Start Visual Website Optimizer Asynchronous Code -->
    <script type='text/javascript'>
        var _vwo_code = (function () {
            var account_id = 112140,
                settings_tolerance = 2000,
                library_tolerance = 2500,
                use_existing_jquery = false,
                /* DO NOT EDIT BELOW THIS LINE */
                f = false, d = document;
            return {
                use_existing_jquery: function () {
                    return use_existing_jquery;
                }, library_tolerance: function () {
                    return library_tolerance;
                }, finish: function () {
                    if (!f) {
                        f = true;
                        var a = d.getElementById('_vis_opt_path_hides');
                        if (a) a.parentNode.removeChild(a);
                    }
                }, finished: function () {
                    return f;
                }, load: function (a) {
                    var b = d.createElement('script');
                    b.src = a;
                    b.type = 'text/javascript';
                    b.innerText;
                    b.onerror = function () {
                        _vwo_code.finish();
                    };
                    d.getElementsByTagName('head')[0].appendChild(b);
                }, init: function () {
                    settings_timer = setTimeout('_vwo_code.finish()', settings_tolerance);
                    var a = d.createElement('style'),
                        b = 'body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',
                        h = d.getElementsByTagName('head')[0];
                    a.setAttribute('id', '_vis_opt_path_hides');
                    a.setAttribute('type', 'text/css');
                    if (a.styleSheet) a.styleSheet.cssText = b; else a.appendChild(d.createTextNode(b));
                    h.appendChild(a);
                    this.load('//dev.visualwebsiteoptimizer.com/j.php?a=' + account_id + '&u=' + encodeURIComponent(d.URL) + '&r=' + Math.random());
                    return settings_timer;
                }
            };
        }());
        _vwo_settings_timer = _vwo_code.init();
    </script>
    <!-- End Visual Website Optimizer Asynchronous Code -->
</head>
<header class="pad-vert-2x hidden-xs">
    <div class="container">
        <div class="row">
            <div class="col-sm-5 col-md-4">
                <a href="<?= esc_url(home_url('/')); ?>">
                    <picture>
                        <source srcset="<?php bloginfo('stylesheet_directory'); ?>/assets/main-logo.png"/>
                        <img id="mainlogo-desktop" class="img-responsive main-logo"
                             src="<?php bloginfo('stylesheet_directory'); ?>/assets/main-logo.png"
                             alt="logo">
                    </picture>
                </a>
            </div>
            <div class="col-md-6 col-md-offset-2 text-right">
                <p class="larger margin-x"><a href="tel:<?= do_shortcode('[tel-link]') ?>"><i class="fa fa-mobile"></i>
                        <strong><?= do_shortcode('[service-tel]') ?></strong></a>
                </p>
                <p><a target="_blank" href="<?= do_shortcode('[location-blink]') ?>"><i
                                class="fa fa-map-marker"></i> <?= do_shortcode('[location-1]') ?>
                        , <?= do_shortcode('[location-2]') ?></a>
                </p>
                <p><?= do_shortcode('[boldchat-btn]') ?></p>
            </div>
        </div>
    </div>
</header>
<nav class="navbar navbar-default" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <i class="fa fa-fw fa-bars"></i>
            </button>
            <a class="navbar-brand visible-xs" href="/">
                <picture>
                    <source srcset="<?php bloginfo('stylesheet_directory'); ?>/assets/main-logo-xs.png"
                            media="(max-width: 767px)"/>
                    <source srcset="#" media="(min-width: 768px)"/>
                    <img id="mainlogo-mobile" src="<?php bloginfo('stylesheet_directory'); ?>/assets/main-logo-xs.png"
                         alt="logo"
                         class="img-responsive main-logo">
                </picture>
            </a>
        </div>
        <?php
        wp_nav_menu(array(
                'menu' => 'main-nav',
                'theme_location' => 'primary',
                'depth' => 2,
                'container' => 'div',
                'container_class' => 'collapse navbar-collapse',
                'container_id' => 'bs-example-navbar-collapse-1',
                'menu_class' => 'nav navbar-nav',
                'fallback_cb' => 'navwalker::fallback',
                'walker' => new navwalker()
            )
        );
        ?>
    </div>
</nav>
<body style="opacity:1!important" <?php body_class(); ?>>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=<?= do_shortcode('[analytics-gtmid]') ?>" height="0"
            width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->


