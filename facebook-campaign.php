<?php
/* Template Name: FB Dynamic LP */

get_header(); ?>
<style>
    header.pad-vert-2x.hidden-xs, nav.navbar.navbar-default, footer, div#map, div#cncb11, div#cncb3, div#cnpoke {
        display: none !important
    }

    .smltxt {
        font-size: .8em
    }

    .complimentaryoffer {
        background-color: #e4e4e4 !important;
        color: black !important;
        text-align: left !important;
    }

    ul {
        list-style-type: unset !important;
    }

    canvas {
        padding-left: 0;
        padding-right: 0;
        margin-left: auto;
        margin-right: auto;
        display: block
    }

    canvas {
        display: none !important
    }

    @media only screen and (min-width: 992px) section {
        padding:  0!important
    }

    section {
        padding: 0 !important
    }

    section {
        padding: 0 !important
    }

    .panel {
        box-shadow: unset !important;
        -webkit-border-radius: 0 !important;
        -moz-border-radius: 0 !important;
        border-radius: 0 !important;
    }

</style>

<section id="generatedCanvas">
</section>

<?php
while (have_posts()) : the_post();

    get_template_part('template-parts/content', 'page');
    // Conditional Scripts
    // if (is_page('schedule-service')) {
    //     echo "<script type='text/javascript' src='https://blueshirtcode.com/wp-content/themes/servo/bower_components/PACE/pace.min.js'></script>";
    // }

    // If comments are open or we have at least one comment, load up the comment template.
    if (comments_open() || get_comments_number()) :
        comments_template();
    endif;

endwhile; // End of the loop.
?>
<section class="text-center">
    <div class="col-xs-12">
        <p><a onclick="downloadImage()" id="downloadBtn" class="btn btn-main">Download Coupon Offer <i
                        class="fa fa-fw fa-download fa-lg"></i></a></p>
    </div>
</section>
<?php
get_footer();

?>

<script>
    function downloadImage() {
        html2canvas(document.getElementById("coupon")).then(function (canvas) {
            document.getElementById("generatedCanvas").appendChild(canvas);
            document.getElementsByTagName("canvas")[0];
            canvas.toDataURL("image/png");
            var link = document.createElement('a');
            link.href = canvas.toDataURL("image/png");
            link.download = 'Coupon.png';
            document.body.appendChild(link);
            link.click();
        });
    }
</script>
