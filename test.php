<?php
/* Template Name: Test Page */

// Create an empty array for storage
$post_data = array();
// Set arguments for your query
$args = array(
    'post_type'         => 'coupons',
    'posts_per_page'    => 999
);
// Do our query with any arguments from above
$query = new WP_Query( $args );
if ( $query->have_posts() )
{
    while ( $query->have_posts() )
    {
        $query->the_post();
        // Setup our repeater
        if( have_rows('trip_info') ):
            while ( have_rows('trip_info') ) : the_row();
                // Optional - Only get posts that match a true false within our repeater
                if ( get_sub_field('featured_trip') )
                {
                    // Build our array with the data we need using key => value
                    $post_data[] = array(
                        'title' => get_the_title(),
                        'date'  => get_sub_field('trip_date')
                    );
                }
            endwhile;
        endif;
    }
}
// Custom function to use in our sort to list the items by date
function subval_sort( $a, $b )
{
    if ( $a['date'] == $b['date'] )
        return 0;
    return $a['date'] < $b['date'] ? -1 : 1;
}

// Sort our multidimensional array by sub array value
usort( $post_data, 'subval_sort' );

// We can now work our data normally through easy to access methods
foreach ( $post_data as $post )
{
    echo $post['title'] . '&nbsp; ' ;
    echo $post['date'];
    echo '<br/>';
}
