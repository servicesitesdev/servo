<?php

$DealerName = get_option('dealer_name');
$DealerLocation = get_option('dealer_location2');
$FormatDealerlocation = $firstpart = current(explode(',', $DealerLocation));

switch ($post->post_name) {

    case "home":
        echo '<meta name="Title" content="' . $DealerName . ' Service Center"/><meta name="description" content="Get car repairs & maintenance at our Service Center. We use only original manufacturer parts & certified service technicians so your repairs are done right."/>';
        break;
    case "schedule-service":
        echo '<meta name="Title" content="Schedule Repairs & Maintenance | ' . $FormatDealerlocation . 'Car Repair"/><meta name="description" content="Make an appointment for car repairs, maintenance, or body work online instantly at ' . $DealerName . '\'s' . ' Service Center."/>';
        break;
    case "services":
        echo '<meta name="Title" content="Our Services | ' . $FormatDealerlocation . ' Car Repair"/>
        <meta name="description" content="Learn more about ' . $DealerName . '\'s' . ' service center, including oil changes, alignments, brake repair, and tires."/>';
        break;
    case "oil-fluid-changes":
    case "oil-and-fluid-changes":
        echo '<meta name="Title" content="Get an Oil Change | ' . $FormatDealerlocation . ' Car Repair"/>
        <meta name="description" content="Learn more about oil changes at ' . $DealerName . ', then schedule your oil change appointment."/>';
        break;
    case "batteries":
        echo '<meta name="Title" content="Get Car Batteries Replaced | ' . $FormatDealerlocation . ' Car Repair"/>
        <meta name="description" content="Learn more about car batteries repair and replacement at ' . $DealerName . ', then schedule your appointment."/>';
        break;
    case "tires":
        echo '<meta name="Title" content="Get Tire Rotation, Repair, or Replacement | ' . $FormatDealerlocation . ' Car Repair"/>
        <meta name="description" content="Learn more about tire repairs, replacement, alignment and rotation at ' . $DealerName . ', then schedule your appointment."/>';
        break;
    case "brakes-pads":
        echo '<meta name="Title" content="Get Brake Repairs | ' . $FormatDealerlocation . ' Service Center"/>
        <meta name="description" content="Learn more about brake repair and pad replacement at ' . $DealerName . ', then schedule your oil change appointment."/>';
        break;
    case "parts":
        echo '<meta name="Title" content="Get OEM Parts | ' . $FormatDealerlocation . 'Car Repair"/>
        <meta name="description" content="Learn more about Original Equipment Manufacturer (OEM) parts at ' . $DealerName . '. Purchase online, over the phone, or in-store."/>';
        break;
    case "coupons":
        echo '<meta name="Title" content="Coupons for OEM Parts & Car Repair | ' . $FormatDealerlocation . ' Car Repair"/>
        <meta name="description" content="Get coupons and discounts for OEM parts, car repair, and car maintenance at ' . $DealerName . ' Service Center."/>';
        break;
    case "Learning Center":
        echo '<meta name="Title" content="Learn About Our Services | ' . $FormatDealerlocation . ' Car Repair/>
        <meta name="description" content="Learn more about the services we offer including tires, oil changes, batteries, engine repair and more at ' . $DealerName . ' Service Center."/>';
        break;
    case "faqs":
    case "faq":
        echo '<meta name="Title" content="FAQ | ' . $FormatDealerlocation . ' Car Repair"/>
        <meta name="description" content="Have questions on car repairs, maintenance, and parts? Get answers to common questions at ' . $DealerName . '."/>';
        break;
    case "contact-information":
    case "contact-us":
        echo '<meta name="Title" content="Contact Us | ' . $FormatDealerlocation . ' Car Repair"/>
        <meta name="description" content="Contact our dealership regarding car repairs and service."/>';
        break;
    case "our-staff":
        echo '<meta name="Title" content="Our Staff | ' . $FormatDealerlocation . ' Car Repair"/>
        <meta name="description" content="Want to contact a member of our Service Center\'s staff? See our full staff list here."/>';
        break;
// Specialty Pages
    case "rewards-program" || "hyundai-rewards-program":
        echo '<meta name="Title" content="Rewards Program | ' . $DealerName . '"/>
        <meta name="description" content="Enroll today and earn points towards future repairs & maintenance or your next vehicle purchase at ' . $DealerName . '."/>';
        break;
    case "roadside-assistance":
        echo '<meta name="Title" content="Roadside Assistance | 24/7 |' . $DealerName . '"/>
        <meta name="description" content="Get roadside assistance 24 hours a day, 7 days a week, 365 days a year at ' . $DealerName . '."/>';
        break;
    case "protection-plan":
        echo '<meta name="Title" content="Protection Plan | ' . $DealerName . '"/>
        <meta name="description" content="Protect yourself from costly car repair and maintenance bills with a Hyundai Protection Plan at ' . $DealerName . '."/>';
        break;
    case "prepaid-maintenance":
        echo '<meta name="Title" content="Pre-Paid Maintenance | ' . $DealerName . '"/>
        <meta name="description" content="Pre-pay and save on costly maintenance and repairs for your Mazda at ' . $DealerName . '."/>';
        break;
    case "service-a-service-b":
        echo '<meta name="Title" content="Service A & Service B | ' . $DealerName . ' . "/>
    	 <meta name="description" content="Find out which services are performed for Service A &amp; Service B, then make a car repair appointment."/>';
        break;
    case "total-ownership-experience":
        echo '<meta name="Title" content="Total Ownership Experience | ' . $DealerName . ' . "/>
    	 <meta name="description" content="Get complimentary loaner vehicles, 24/7 roadside assistance, trip interruption benefits, and more through our INFINITI owners\' program."/>';
        break;
    case "loyalty-rewards":
        echo '<meta name="Title" content="Earn Points for Service & Repairs | ' . $DealerName . ' . "/>
    	 <meta name="description" content="For every 300 points you ear, you can get $20 off your next service at ' . $DealerName . '"/>';
        break;
    case "toyotacare":
        echo '<meta name="Title" content="No Cost Maintenance | 24 Hour Roadside Assistance"/>
    	 <meta name="description" content="Learn more about ToyotaCare at Dealer Name and find out if your car is eligible."/>';
        break;
}