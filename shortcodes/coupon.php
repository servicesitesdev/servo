<?php

/* ----------------------------------------------------------------------------- */
/* FEATURED COUPONS */
/* ----------------------------------------------------------------------------- */

function featured_coupon_shortcode()
{
    function filter_by_value($array, $index, $value)
    {
        $newarray = null;
        if (is_array($array) && count($array) > 0) {
            foreach (array_keys($array) as $key) {
                $temp[$key] = $array[$key][$index];

                if ($temp[$key] == $value) {
                    $newarray[$key] = $array[$key];
                }
            }
        }
        return $newarray;
    }

    //General Settings
    $generalSettings = get_field('general_settings', 'option');
    $generalExpiration = $generalSettings['expiration_date'];
    $automaticExpiration = $generalSettings['automatic_expiration_update'];
	$logo = ($generalSettings['logo']['url'] == true ? $logo ='<div class="coupon_logo"><img src="' . $logo = $generalSettings['logo']['url'] . '" />' : false);
    $coupons = get_field('coupons', 'option');
    $featuredCoupon = get_sub_field('featured_coupon', 'option');
    $coupon_bottomDisclaimer = get_sub_field('bottom_disclaimer', 'option');
    $bottomDisclaimer = get_sub_field('bottom_disclaimer', 'option');
    $disclaimerOverride = get_sub_field('disclaimer_override', 'option');
    $expirationOverride = get_sub_field('expiration_date_override', 'option');

    //Filtered Coupon
    $filtered_coupons_array = filter_by_value($coupons, 'featured_coupon', 1);
    if ($filtered_coupons_array) {
        $theme_name = wp_get_theme( $stylesheet, $theme_root );
        //Coupon1
        $coupon_title1 = current($filtered_coupons_array)['title'];
        $coupon_price1 = current($filtered_coupons_array)['price'];
        $coupon_bodyCopy1 = current($filtered_coupons_array)['body_copy'];
        $coupon_spendmore1 = current($filtered_coupons_array)['spend_more_save_more'];
        $coupon_disclaimerOverride1 = current($filtered_coupons_array)['disclaimer_override'];
        $coupon_expiration_date1 = boolval($automaticExpiration) ? "<strong class='expireDate'></strong>" : "<strong> Expires " . date('m/d/Y', strtotime($generalExpiration)) . "</strong>";
        if (current($filtered_coupons_array)['expiration_date_override']) {
            $coupon_expiration_date1 = "<strong> Expires " . date('m/d/Y', strtotime(current($filtered_coupons_array)['expiration_date_override'])) . "</strong>";
        }
        $coupon_disclaimer1 = boolval($disclaimerOverride) ? get_sub_field('disclaimer_override', 'option') : $generalSettings['bottom_disclaimer'];
        if (current($filtered_coupons_array)['bottom_disclaimer']) {
            $coupon_disclaimer1 = current($filtered_coupons_array)['bottom_disclaimer'];
        }
        $coupon_offer_disclaimer1 = (current($filtered_coupons_array)['disclaimer_override']) ? current($filtered_coupons_array)['disclaimer_override'] : $generalSettings['general_disclaimer'];
        //Coupon2
        $coupon_title2 = next($filtered_coupons_array)['title'];
        $coupon_price2 = current($filtered_coupons_array)['price'];
        $coupon_bodyCopy2 = current($filtered_coupons_array)['body_copy'];
        $coupon_spendmore2 = current($filtered_coupons_array)['spend_more_save_more'];
        $coupon_disclaimerOverride2 = current($filtered_coupons_array)['disclaimer_override'];
        $coupon_expiration_date2 = boolval($automaticExpiration) ? "<strong class='expireDate'></strong>" : "<strong> Expires " . date('m/d/Y', strtotime($generalExpiration)) . "</strong>";
        if (current($filtered_coupons_array)['expiration_date_override']) {
            $coupon_expiration_date2 = "<strong> Expires " . date('m/d/Y', strtotime(current($filtered_coupons_array)['expiration_date_override'])) . "</strong>";
        }
        $coupon_disclaimer2 = boolval($disclaimerOverride) ? get_sub_field('disclaimer_override', 'option') : $generalSettings['bottom_disclaimer'];
        if (current($filtered_coupons_array)['bottom_disclaimer']) {
            $coupon_disclaimer2 = current($filtered_coupons_array)['bottom_disclaimer'];
        }
        $coupon_offer_disclaimer2 = (current($filtered_coupons_array)['disclaimer_override']) ? current($filtered_coupons_array)['disclaimer_override'] : $generalSettings['general_disclaimer'];
        //Coupon3
        $coupon_title3 = next($filtered_coupons_array)['title'];
        $coupon_price3 = current($filtered_coupons_array)['price'];
        $coupon_bodyCopy3 = current($filtered_coupons_array)['body_copy'];
        $coupon_spendmore3 = current($filtered_coupons_array)['spend_more_save_more'];
        $coupon_disclaimerOverride3 = current($filtered_coupons_array)['disclaimer_override'];
        $coupon_expiration_date3 = boolval($automaticExpiration) ? "<strong class='expireDate'></strong>" : "<strong> Expires " . date('m/d/Y', strtotime($generalExpiration)) . "</strong>";
        if (current($filtered_coupons_array)['expiration_date_override']) {
            $coupon_expiration_date3 = "<strong> Expires " . date('m/d/Y', strtotime(current($filtered_coupons_array)['expiration_date_override'])) . "</strong>";
        }
        $coupon_disclaimer3 = boolval($disclaimerOverride) ? get_sub_field('disclaimer_override', 'option') : $generalSettings['bottom_disclaimer'];
        if (current($filtered_coupons_array)['bottom_disclaimer']) {
            $coupon_disclaimer3 = current($filtered_coupons_array)['bottom_disclaimer'];
        }
        $coupon_offer_disclaimer3 = (current($filtered_coupons_array)['disclaimer_override']) ? current($filtered_coupons_array)['disclaimer_override'] : $generalSettings['general_disclaimer'];
        //Debugging
        //print "<pre>";
        //print_r($filtered_coupons_array);
        //print "</pre>";
        //Initialize Coupon vars
        $featuredCoupon1 = " ";
        $featuredCoupon2 = " ";
        $featuredCoupon3 = " ";
        if (reset($filtered_coupons_array)['featured_coupon']) {
            $featuredCoupon1 = <<<C1
                           <div id="coupon1">
                                    <div class="cell text-center center-block coupon">
                                    <h2 class="coupon-lineheight">{$coupon_title1}</h2>
                                    <p class="price margin-bottom-x coupon-lineheight"><strong>{$coupon_price1}</strong></p>
                                    <br>
                                    <p class="coupon-description">{$coupon_bodyCopy1}</p>
                                    <hr>
                                    <p class="disclaimer">{$coupon_offer_disclaimer1}{$coupon_expiration_date1}</p>
                                    <p class="text-primary"><strong>{$coupon_disclaimer1}</strong></p>
                                </div>
                            </div>
                            <p class="margin-top-2x"><a class="btn btn-main-outline" target="_blank" onclick="printCoupon('coupon1')"><i class="fa fa-print"></i> Print Coupon</a>
C1;
        }
        if (next($filtered_coupons_array)['featured_coupon']) {
            $featuredCoupon2 = <<<C2
                            <div id="coupon2">
                                    <div class="cell text-center center-block coupon">
                                    <h2 class="coupon-lineheight">{$coupon_title2}</h2>
                                    <p class="price margin-bottom-x coupon-lineheight"><strong>{$coupon_price2}</strong></p>
                                    <br>
                                    <p class="coupon-description">{$coupon_bodyCopy2}</p>
                                    <hr>
                                    <p class="disclaimer">{$coupon_offer_disclaimer2}{$coupon_expiration_date2}</p>
                                    <p class="text-primary"><strong>{$coupon_disclaimer2}</strong></p>
                                </div>
                            </div>
                            <p class="margin-top-2x"><a class="btn btn-main-outline" target="_blank" onclick="printCoupon('coupon2')"><i class="fa fa-print"></i> Print Coupon</a></p>
C2;
        }
        if (next($filtered_coupons_array)['featured_coupon']) {
            $featuredCoupon3 = <<<C3
                            <div id="coupon3">
                                    <div class="cell text-center center-block coupon">
                                    <h2 class="coupon-lineheight">{$coupon_title3}</h2>
                                    <p class="price margin-bottom-x coupon-lineheight"><strong>{$coupon_price3}</strong></p>
                                    <br>
                                    <p class="coupon-description">{$coupon_bodyCopy3}</p>
                                    <hr>
                                    <p class="disclaimer">{$coupon_offer_disclaimer3}{$coupon_expiration_date3}</p>
                                    <p class="text-primary"><strong>{$coupon_disclaimer3}</strong></p>
                                </div>
                            </div>
                            <p class="margin-top-2x"><a class="btn btn-main-outline" target="_blank" onclick="printCoupon('coupon3')"><i class="fa fa-print"></i> Print Coupon</a></p>
C3;
        }

        if (reset($filtered_coupons_array)['featured_coupon']) {
            return $couponGenerator = <<<CG
    <style>
    canvas {
    padding-left: 0;
    padding-right: 0;
    margin-left: auto;
    margin-right: auto;
    display: block
    }
    canvas {
    display: none!important
    }
    
    /*.coupon-container,*/
    /*.coupon-container > [class*="col"],*/
    /*.coupon-container [id*="coupon"],*/
    /*.coupon-container .coupon*/
    /*{*/
        /*display: -webkit-box;*/
        /*display: -moz-box;*/
        /*display: -ms-flexbox;*/
        /*display: -webkit-flex;*/
        /*display: flex;*/
    /*}*/
    
    /*.coupon-container {*/
        /*-webkit-flex-wrap: wrap;*/
        /*flex-wrap: wrap;*/
        /*-webkit-align-items: stretch;*/
        /*align-items: stretch;*/
    /*}*/
    
    /*.coupon-container > [class*="col"] {*/
        /*-webkit-flex-direction: column;*/
        /*flex-direction: column;*/
        /*-webkit-justify-content: space-between;*/
        /*justify-content: space-between;*/
    /*}*/
    
    /*.coupon-container [id*="coupon"] {*/
        /*flex-grow: 1;*/
    /*}*/
    
    /*.coupon-container .coupon {*/
        /*-webkit-flex-direction: column;*/
        /*flex-direction: column;*/
        /*-webkit-justify-content: space-between;*/
        /*justify-content: space-between;*/
    /*}*/
</style>
    <div id="generatedCanvas"></div>
    <section class="text-center coupons">
        <div class="container">
            <h2 class="featured-coupon-header">We Service and Offer Coupons for All Makes and Models</h2>
            <div class="row coupon-container ">
                <div class="col-lg-12 text-center">
                    <div class="row coupon-container">
                        <div class="col-md-4 col-sm-6 col-sm-uncentered col-xs-centered col-xxs-12 col-xs-8 margin-bottom-2x">
                        {$featuredCoupon1}
                        </div>
                        <div class="col-md-4 col-sm-6 col-sm-uncentered col-xs-centered col-xxs-12 col-xs-8 margin-bottom-2x">
                        {$featuredCoupon2}
                        </div>
                        <div class="col-md-4 col-sm-6 col-sm-uncentered col-xs-centered col-xxs-12 col-xs-8 margin-bottom-2x">
                        {$featuredCoupon3}
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <p><a class="btn btn-main featured-coupon-cta" href="/coupons">View All Coupons<i class="fa fa-fw fa-angle-double-right fa-lg"></i></a></p>
                </div>
            </div>
        </div>
        <script>$(".cell.text-center.center-block.coupon").prepend('{$logo}');</script>
    </section>
CG;
        }
    } else {
        return "<section><div class=\"container\"><div class=\"row margin-bottom-2x\"><div class=\"col-lg-12 text-center\"><div class=\"row\"><p>There are no specials at this time, please check again later for savings</p></div></div></div></div></section>";
    }

}

add_shortcode('featured-coupons', 'featured_coupon_shortcode');