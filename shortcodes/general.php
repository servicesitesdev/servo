<?php

/* ----------------------------------------------------------------------------- */
/* GENERAL */
/* ----------------------------------------------------------------------------- */

function dealer_name_shortcode()
{
    $options = get_option('dealer_name');
    return $options;
}

add_shortcode('dealer-name', 'dealer_name_shortcode');

function mainsite_link_shortcode()
{
    $options = get_option('dealer_mainsite');
    return $options;
}

add_shortcode('mainsite-link', 'mainsite_link_shortcode');

function service_tel_shortcode()
{
    $options = get_option('dealer_tel');
    return $options;
}

add_shortcode('service-tel', 'service_tel_shortcode');

function tel_link_shortcode()
{
    $options = get_option('dealer_tellink');
    return $options;
}

add_shortcode('tel-link', 'tel_link_shortcode');

function location_link_shortcode()
{
    $options = get_option('dealer_locationlink');
    return $options;
}

add_shortcode('location-link', 'location_link_shortcode');

function location_blink_shortcode()
{
    $options = get_option('dealer_gmblink');
    return $options;
}

add_shortcode('location-blink', 'location_blink_shortcode');

function location_1_shortcode()
{
    $options = get_option('dealer_location1');
    return $options;
}

add_shortcode('location-1', 'location_1_shortcode');

function location_2_shortcode()
{
    $options = get_option('dealer_location2');
    return $options;
}

add_shortcode('location-2', 'location_2_shortcode');

function lat_option_shortcode()
{
    $options = get_option('lat_option');
    return $options;
}

add_shortcode('lat-option', 'lat_option_shortcode');

function lng_option_shortcode()
{
    $options = get_option('lng_option');
    return $options;
}

add_shortcode('lng-option', 'lng_option_shortcode');

function service_hours_shortcode()
{
    $options = get_option('dealer_servicehours');
    return $options;
}

add_shortcode('service-hours', 'service_hours_shortcode');

function parts_hours_shortcode()
{
    $options = get_option('dealer_partshours');
    return $options;
}

add_shortcode('parts-hours', 'parts_hours_shortcode');

function parts_link_shortcode()
{
    $options = get_option('dealer_partslink');

    if (get_option('dealer_partslink') != '') {
        return $options;
    } else {
        return '/order-parts';
    }
}

add_shortcode('parts-link', 'parts_link_shortcode');

function accessories_link_shortcode()
{
    $options = get_option('dealer_accessories');

    if (get_option('dealer_accessories') != '') {
        return $options;
    } else {
        return '/order-accessories';
    }
}

add_shortcode('accessories-link', 'accessories_link_shortcode');

function tires_link_shortcode()
{

    $options = get_option('dealer_tireslink');

    if (get_option('dealer_tireslink') != '') {
        return $options;
    } else {
        return '/order-tires';
    }
}

add_shortcode('tires-link', 'tires_link_shortcode');


/* ----------------------------------------------------------------------------- */
/* Coupons */
/* ----------------------------------------------------------------------------- */

function coupon_expiredate_shortcode()
{
    $options = get_option('coupon_expiredate');
    return $options;
}

add_shortcode('coupon-expiredate', 'coupon_expiredate_shortcode');

/* ----------------------------------------------------------------------------- */
/* Custom Code */
/* ----------------------------------------------------------------------------- */

function customcode_header_shortcode()
{
    $options = get_option('customcode_header');
    return $options;
}

add_shortcode('customcode-header', 'customcode_header_shortcode');

function customcode_footer_shortcode()
{
    $options = get_option('customcode_footer');
    return $options;
}

add_shortcode('customcode-footer', 'customcode_footer_shortcode');

/* ----------------------------------------------------------------------------- */
/* Analytics */
/* ----------------------------------------------------------------------------- */

function analytics_gtmid_shortcode()
{
    $options = get_option('gtm_id');
    return $options;
}

add_shortcode('analytics-gtmid', 'analytics_gtmid_shortcode');

function meta_google_shortcode()
{
    $options = get_option('meta_google');
    return $options;
}

add_shortcode('meta-google', 'meta_google_shortcode');

function boldchat_btn_shortcode()
{
    $options = get_option('boldchat_btn');
    return $options;
}

add_shortcode('boldchat-btn', 'boldchat_btn_shortcode');

function vwo_accountid_shortcode()
{
    $options = get_option('vwo_accountid');
    return $options;
}

/* ----------------------------------------------------------------------------- */
/* GALLERY */
/* ----------------------------------------------------------------------------- */

function service_gallery_shortcode()
{
    return '<section class="text-center bg-lgt text-white visible-sm visible-xs"><div class=container><div class="row usps"><div class="col-sm-6 col-md-4"><div class="panel panel-x margin-vert-1x usp-1"><div class="cell pad-horz-2x"><h2>Complimentary Battery Check</h2><p class="text-white thin">Actuate the positive, eliminate the negative.</div></div></div><div class="col-sm-6 col-md-4"><div class="panel panel-x margin-vert-1x usp-2"><div class="cell pad-horz-2x"><h2>Complimentary Shuttle Service</h2><p class="text-white thin">You can even pick the radio station.</div></div></div><div class="col-sm-6 col-md-4"><div class="panel panel-x margin-vert-1x usp-3"><div class="cell pad-horz-2x"><h2>Complimentary Tire Fill &amp; Check with Service</h2><p class="text-white thin">Because you\'re under enough pressure.</div></div></div><div class="col-sm-6 col-md-4"><div class="panel panel-x margin-vert-1x usp-4"><div class="cell pad-horz-2x"><h2>Complimentary Car Wash with Service*</h2><p class="text-white thin">*Rubber ducky not included.</div></div></div><div class="col-sm-6 col-md-4"><div class="panel panel-x margin-vert-1x usp-5"><div class="cell pad-horz-2x"><h2>Oil change 45 min or less</h2><p class="text-white thin">A pit stop featuring free wifi and coffee.</div></div></div><div class="col-sm-6 col-md-4"><div class="panel panel-x margin-vert-1x usp-9"><div class="cell pad-horz-2x"><h2>Complimentary Loaner Vehicles</h2><p class="text-white thin">It may not be a new car, but it\'ll smell like one.</div></div></div><div class="col-sm-6 col-md-4"><div class="panel panel-x margin-vert-1x usp-7"><div class=cell><h2>Complimentary Alignment Check</h2><p class="text-white thin">Think chiropractic for your car.</div></div></div></div></div> </section><div class="text-center carousel hidden-sm hidden-xs slide" data-ride=carousel id=carousel-example-generic> <section class="text-white bg-lgt"><div class=carousel-inner role=listbox><div class="item active"><div class=container><div class="row usps"><div class=col-md-4><div class="panel panel-x margin-vert-1x usp-1"><div class=cell><h2>Complimentary Battery Check</h2><p class="text-white thin">Actuate the positive, eliminate the negative.</div></div></div><div class=col-md-4><div class="panel panel-x margin-vert-1x usp-2"><div class=cell><h2>Complimentary Shuttle Service</h2><p class="text-white thin">You can even pick the radio station.</div></div></div><div class=col-md-4><div class="panel panel-x margin-vert-1x usp-3"><div class=cell><h2>Complimentary Tire Fill &amp; Check with Service</h2><p class="text-white thin">Because you\'re under enough pressure.</div></div></div></div></div></div><div class=item><div class=container><div class="row usps"><div class=col-md-6><div class="panel panel-x margin-vert-1x usp-4"><div class=cell><h2>Complimentary Car Wash with Service*</h2><p class="text-white thin">*Rubber ducky not included.</div></div></div><div class=col-md-6><div class="panel panel-x margin-vert-1x usp-5"><div class=cell><h2>Oil change 45 min or less</h2><p class="text-white thin">A pit stop featuring free wifi and coffee.</div></div></div></div></div></div><div class=item><div class=container><div class="row usps"><div class=col-md-6><div class="panel panel-x margin-vert-1x usp-9"><div class=cell><h2>Complimentary Loaner Vehicles</h2><p class="text-white thin">It may not be a new car, but it\'ll smell like one.</div></div></div><div class=col-md-6><div class="panel panel-x margin-vert-1x usp-7"><div class=cell><h2>Complimentary Alignment Check</h2><p class="text-white thin">Think chiropractic for your car.</p></div></div></div></div></div></div></div> <a class="carousel-control left" href=#carousel-example-generic data-slide=prev role=button><span class="glyphicon glyphicon-chevron-left"aria-hidden=true></span><span class=sr-only>Previous</span></a><a class="carousel-control right" href=#carousel-example-generic data-slide=next role=button><span class="glyphicon glyphicon-chevron-right"aria-hidden=true></span><span class=sr-only>Next</span></a> </section></div>';
}

add_shortcode('service-gallery', 'service_gallery_shortcode');

// myKarma Banner
function karma_banner_shortcode()
{
    return '<section><div class="container mykaarma text-center-xxs"><div class=row><div class="col-md-centered col-lg-8 col-md-10"><h2 class=color-mykaarma>Save Time / Pay by Phone</h2><p class="thin h4 margin-bottom-3x">Prefer texting to phone calls? No problem. We can send you a text when we\'re done or if we find any problems. When you\'re ready to pick up your vehicle you can skip the service line and pay from your phone, tablet or laptop.</div></div><div class="row margin-bottom-2x steps"><div class=col-sm-4><img alt="Secd us your cell" class=img-responsive src=/wp-content/themes/servo/assets/mykaarma-step1.png><h3>Step 1</h3><p>Holla! Send us your cell number so we can text. Less talk, more action!</div><div class=col-sm-4><hr class="visible-xs margin-vert-4x sm"><img alt="We\'ll send you updates" class=img-responsive src=/wp-content/themes/servo/assets/mykaarma-step2.png><h3>Step 2</h3><p>As work progresses, we\'ll send updates. No news is good news, but like the saying goes: if we see something, we\'ll say something.<hr class="visible-xs margin-vert-4x sm"></div><div class=col-sm-4><img alt="Your car is ready" class=img-responsive src=/wp-content/themes/servo/assets/mykaarma-step3.png><h3>Step 3</h3><p>Your car is ready! Here\'s the bill. Skip the line and pay online!</div></div></div> </section>';
}

add_shortcode('karma-banner', 'karma_banner_shortcode');

// One to One Rewards Banner
function one2one_banner_shortcode()
{
    return '<div id="one-to-one"> <section class="banner screen-50"><div class="cell"><div class="container"><div class="row text-center"><div class="col-md-8 col-md-centered text-white"><p class="lead thin margin-x"></p><h1 class="margin-bottom-2x">One To One Rewards</h1><div class="row"><div class="col-sm-6 text-right-sm"><h3><strong>Earn Rewards Points</strong></h3><p class="thin">For routine maintenance you already do.</p></div><div class="col-sm-6 text-left-sm"><h3><strong>Redeem Reward Points</strong></h3><p class="thin">Lower your cost of ownership by using your points on services you need.</p></div></div><p class="visible-xxs"><a href="/services/one-to-one-rewards/" class="btn btn-block btn-main">Get the Details</a> <a href="https://www.nissanonetoonerewards.com/OnlineEnrollment" target="_blank" class="btn btn-block btn-white-outline hidden-md hidden-lg">Enroll Today!</a></p><div class="row hidden-xxs"><div class="col-sm-6 text-right-sm"><p><a href="/services/one-to-one-rewards/" class="btn btn-main">Get the Details</a></p></div><div class="col-sm-6 text-left-sm"><p> <a type="button" onclick="popupModal(\'https://www.nissanonetoonerewards.com/OnlineEnrollment\', \'\', \'1000\', \'800\')" class="btn btn-white-outline">Enroll Today!</a></p></div></div></div></div></div></div> </section></div>';
}

add_shortcode('one2one-banner', 'one2one_banner_shortcode');

// Carfax Banner
function carfax_banner_shortcode()
{
    return '<div id=carfax> <section class="header screen-50"><div class=cell><div class=container><div class="row text-center"><div class="text-white col-md-6 visible-md visible-lg"></div><div class="margin-bottom-3x text-white text-left-md col-xs-12 col-md-6"><h2 class="margin-top-2x margin-bottom-1x carfax-txt"><b>Does Your Car Have a Safety Recall?</b></h2><p class="thin carfax-bodytxt"> Recalls have been in the news a lot lately. We know you have questions about the effect of these recalls on the safety of you and your passengers. We can\'t service every vehicle with an open recall, but we can alert you if your vehicle has one.</p><h4 class="carfax-bodytxt"><b>Find out if your car is affected.</b></h4><p></p><p><a class="btn carfax-btn hidden-xs hidden-sm" onclick="popupModal(\'https://www.mycarfax.com\', \'\', \'1000\', \'800\')">Check My Car</a> <a class="btn carfax-btn hidden-md hidden-lg" target="_blank" href="https://www.mycarfax.com">Check My Car</a></p> <img width="100px;" src="/wp-content/themes/servo/assets/mycarfax-logo.png"></div></div></div></div> </section></div>';
}

add_shortcode('carfax-banner', 'carfax_banner_shortcode');

//  Certified Technicians
function certifiedtech_banner_shortcode()
{
    return 'section><div class="container text-center"><h2 class="margin-bottom-2x">Certified Technicians</h2><div class="row text-right-md margin-bottom-2x"><div class="col-sm-10 col-sm-centered col-md-12"><div class="row"><div class="col-md-6"><p>Who knows your Nissan better than your local Nissan dealer? Our ASE Certified Technicians are highly trained and knowledgeable. They have the skills, tools and experience needed to give your vehicle the care it deserves. Trust your Nissan with us. It\'s in good hands.</p></div><div class="col-xs-6 col-xxs-centered col-md-uncentered"><img class="img-responsive margin-bottom-1x ase" alt="certified mechanic" src="h/wp-content/themes/servo/assets/ase-certified-mechanic.png" /></div></div></div></div></div> </section>';
}

add_shortcode('certifiedtech-banner', 'certifiedtech_banner_shortcode');


