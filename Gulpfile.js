'use strict';

const gulp = require('gulp'),
    sass = require('gulp-sass'),
    cleanCSS = require('gulp-clean-css'),
    babel = require('gulp-babel'),
    include = require('gulp-include'),
    uglify = require('gulp-uglify'),
    watch = require('gulp-watch');
    // ts = require('gulp-typescript');

//Styles
gulp.task('styles', function () {
    return gulp.src('./dev/styles/style.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(include())
        .pipe(cleanCSS({
            compatibility: 'ie8',
            specialComments: 'all'
        }))
        .pipe(gulp.dest('./'));
});

// ES6/TypeScript
gulp.task("scripts", function () {
    // TypeScript Compiler Action
    // gulp.src('./dev/scripts/typescript.ts')
    //     .pipe(ts({
    //         noImplicitAny: true,
    //         out: 'typescript.js'
    //     }))
    return gulp.src('./dev/scripts/es6.js')
    .pipe(babel({
        presets: ['es2015']
    }))
        .on('error', console.log)
        .pipe(uglify())
        .pipe(gulp.dest("./dev/scripts/public/"))
});

// Combine Scripts
gulp.task("scriptHandler", function () {
    return gulp.src('./dev/scripts/main.js')
        .pipe(include())
            .on('error', console.log)
        .pipe(gulp.dest("./"))
});

// Watch for changes
gulp.task('watch', function () {
    gulp.watch('./dev/styles/*.scss', ['styles']);
    gulp.watch('./dev/scripts/es6.js', ['scripts']);
    gulp.watch('./dev/scripts/main.js', ['scripts', 'scriptHandler']);
});

gulp.task('default', ['styles', 'scripts', 'scriptHandler', 'watch']);
