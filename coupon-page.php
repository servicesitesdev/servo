<?php
/* Template Name: Coupon Page */

get_header(); ?>
<style>
    .coupon-container,
    .coupon-container > [class*="col"],
    .coupon-container [id*="coupon"],
    .coupon-container .coupon
    {
        display: -webkit-box;
        display: -moz-box;
        display: -ms-flexbox;
        display: -webkit-flex;
        display: flex;
    }

    .coupon-container {
        -webkit-flex-wrap: wrap;
        flex-wrap: wrap;
        -webkit-align-items: stretch;
        align-items: stretch;
    }

    .coupon-container > [class*="col"] {
        -webkit-flex-direction: column;
        flex-direction: column;
        -webkit-justify-content: space-between;
        justify-content: space-between;
    }

    .coupon-container [id*="coupon"] {
        flex-grow: 1;
        flex-direction: column;
	    -webkit-flex-direction: column;
    }

    .coupon-container .coupon {
        -webkit-flex-direction: column;
        flex-direction: column;
        -webkit-justify-content: space-between;
        justify-content: space-between;
    }

    [id*="coupon"] hr {
        width:100%;
    }

    [id*="coupon"] .coupon.cell {
        height:auto !important;
        flex-grow: 1;
    }
</style>
<div id="coupons">
    <section class="bg-drk">
        <div class="container text-center">
            <div class="row">
                <div class="col-md-10 col-md-centered col-lg-8">
                    <h1>Coupons</h1>
                    <p class="lead thin">We pride ourselves on our everyday low prices on service and maintenance
                        <br class="hidden-xs"><strong class="text-primary">for all makes and models</strong>.</p>
                </div>
            </div>
        </div>
    </section>
</div>

<?php
$generalSettings     = get_field( 'general_settings', 'option' );
$generalExpiration   = $generalSettings['expiration_date'];
$automaticExpiration = $generalSettings['automatic_expiration_update'];
$logo                = $generalSettings['logo']['url'];
$coupons             = get_field( 'coupons', 'option' );
?>

<?php if ( have_rows( 'coupons', 'option' ) ): $i = 0; ?>

    <section>
        <div class="container">
            <div class="row margin-bottom-2x">
                <div class="col-lg-12 text-center">
                    <div class="coupon-container row">

						<?php while ( have_rows( 'coupons', 'option' ) ): the_row();
							$i ++ ?>

							<?php
							$today              = date( 'Ymd' );
							$coupons            = get_field( 'coupons', 'option' );
							$title              = get_sub_field( 'title', 'option' );
							$price              = get_sub_field( 'price', 'option' );
							$expired            = null;
							$bodyCopy           = get_sub_field( 'body_copy', 'option' );
							$spendSave          = get_sub_field( 'spend_more_save_more', 'option' );
							$disclaimerOverride = get_sub_field( 'disclaimer_override', 'option' );
							$expirationOverride = get_sub_field( 'expiration_date_override', 'option' );
							$bottomDisclaimer   = get_sub_field( 'bottom_disclaimer', 'option' );
							$topField           = get_sub_field( 'top_field', 'option' );
							$featuredCoupon     = get_sub_field( 'featured_coupon', 'option' );
							$theme_name         = wp_get_theme( $stylesheet, $theme_root );
							?>

							<?php

							if ( $expirationOverride and ! $automaticExpiration ):

								if ( $expirationOverride < $today ):

									$expired = true;

								endif;

                            elseif ( $generalExpiration and ! $automaticExpiration ):

								if ( $generalExpiration < $today ):

									$expired = true;

								endif;

							endif;

							?>

							<?php if ( $expired != true ): ?>
                                <div class="col-md-4 col-sm-6 col-sm-uncentered col-xs-centered col-xxs-12 col-xs-8 margin-bottom-2x">
                                    <div id="<?php echo 'coupon';
									echo $i; ?>">
                                        <div class="cell text-center center-block coupon">
											<?php if ( $logo == true ): ?>
                                                    <div class="coupon_logo"><img src="<?= $logo ?>"/></div>
                                            <?php endif; ?>
											<?php if ( $topField == 'title' ): ?>
												<?php if ( $title ): ?>
                                                    <h2 class="coupon-lineheight"><?php echo $title; ?></h2>
												<?php endif; ?>
											<?php endif; ?>
											<?php if ( $price ): ?>
                                                <p class="price margin-bottom-x coupon-lineheight">
                                                    <strong><?php echo $price; ?></strong></p>
											<?php endif; ?>
											<?php if ( $topField == 'price' ): ?>
												<?php if ( $title ): ?>
                                                    <h2 class="coupon-lineheight"><?php echo $title; ?></h2>
												<?php endif; ?>
											<?php endif; ?>
											<?php if ( $bodyCopy ): ?>
                                                <p class="coupon-description"><?php echo $bodyCopy; ?></p>
												<?php if ( ! $spendSave ): ?>
                                                    <hr><?php endif; ?>
											<?php endif; ?>
											<?php if ( $spendSave ): ?>
                                                <table class="table table-striped table-bordered">
													<?php foreach ( $spendSave as $tableRow ): ?>
                                                        <tr>
															<?php foreach ( $tableRow as $tableCell ): ?>
                                                                <td><?php echo $tableCell; ?></td>
															<?php endforeach; ?>
                                                        </tr>
													<?php endforeach; ?>
                                                </table>
                                                <hr>
											<?php endif; ?>
											<?php if ( $disclaimerOverride ): ?>
                                        <p class="disclaimer"><?php echo $disclaimerOverride; ?>
										<?php elseif ( $generalSettings['general_disclaimer'] ): ?>
                                            <p class="disclaimer"><?php echo $generalSettings['general_disclaimer']; ?>
												<?php endif; ?>
												<?php if ( $expirationOverride ): ?>
                                                <br>
                                                <strong>Expires <?php echo date( 'm/d/Y', strtotime( $expirationOverride ) ); ?></strong>
                                            </p>
										<?php elseif ( $automaticExpiration ): ?>
                                            <br>
                                            <strong class="expireDate"></strong></p>
										<?php elseif ( $generalExpiration ): ?>
                                            <br>
                                            <strong>
                                                Expires <?php echo date( 'm/d/Y', strtotime( $generalExpiration ) ); ?></strong></p>
										<?php endif; ?>
											<?php if ( $bottomDisclaimer ): ?>
                                                <p class="text-primary">
                                                    <strong><?php echo $bottomDisclaimer; ?></strong></p>
											<?php elseif ( $generalSettings['bottom_disclaimer'] ): ?>
                                                <p class="text-primary">
                                                    <strong><?php echo $generalSettings['bottom_disclaimer']; ?></strong>
                                                </p>
											<?php endif; ?>
                                        </div>
                                    </div>
                                    <p class="margin-top-2x"><a class="btn btn-main-outline" target="_blank"
                                                                onclick="printCoupon('<?php echo 'coupon';
									                            echo $i ?>')"><i class="fa fa-print"></i> Print
                                            Coupon</a></p>
                                </div>
							<?php endif; ?>
							<?php $expired = 'reset'; ?>

						<?php endwhile; ?>

                    </div>
                </div>
            </div>
        </div>
    </section>

<?php endif; ?>

<?php
while ( have_posts() ) : the_post();

	get_template_part( 'template-parts/content', 'page' );

endwhile; // End of the loop.
?>

<?php
get_footer();

?>

