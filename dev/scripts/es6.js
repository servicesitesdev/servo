/* Main Site  */

// Native Print Coupon Function

function printCoupon(coupon) {
    var content = document.getElementById(coupon).innerHTML;
    $('<iframe>', {
        name: 'printIframe',
        class: 'printFrame'
    }).appendTo('body').contents().find('body').append(`<html lang="en-US"><head><meta charset="utf-8"><meta name="viewport"content="width=device-width, initial-scale=1, shrink-to-fit=no"><meta http-equiv="x-ua-compatible"content="ie=edge"><link rel='stylesheet'href='https://fonts.googleapis.com/css?family=Roboto:300,100,700'type='text/css'><style>.btn,.text-center{text-align:center}html{font-family:sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%}section{display:block}hr{height:0;-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;margin-top:20px;margin-bottom:20px;border:0;border-top:1px solid#eee}input{margin:0;font:inherit}html input[type=button]{-webkit-appearance:button}input::-moz-focus-inner{padding:0;border:0}*,:after,:before{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box}html{font-size:10px}body{margin:0;font-family:"Helvetica Neue",Helvetica,Arial,sans-serif;font-size:14px;line-height:1.42857143;color:#333;background-color:#fff}input{font-size:inherit;color:#fff}.container{padding-right:15px;padding-left:15px;margin-right:auto;margin-left:auto}.text-primary{color:#333!important}@media(min-width:768px){.container{width:750px}}@media(min-width:992px){.container{width:970px}}@media(min-width:1200px){.container{width:1170px}}.row{margin-right:-15px;margin-left:-15px}.col-lg-4,.col-md-4,.col-sm-6,.col-xs-8{position:relative;min-height:1px;padding-right:15px;padding-left:15px}.col-xs-8{float:left;width:66.66666667%}@media(min-width:768px){.col-sm-6{float:left;width:50%}}@media(min-width:992px){.col-md-4{float:left;width:33.33333333%}}@media(min-width:1200px){.col-lg-4{float:left;width:33.33333333%}}.btn{display:inline-block;margin-bottom:0;font-size:14px;white-space:nowrap;vertical-align:middle;-ms-touch-action:manipulation;touch-action:manipulation;background-image:none;border:1px solid transparent;border-radius:4px}.coupon,p{margin:0 0 1rem}.container:after,.container:before,.row:after,.row:before{display:table;content:" "}.container:after,.row:after{clear:both}@-ms-viewport{width:device-width}.btn,input,p{font-family:Roboto,sans-serif;font-weight:300;line-height:140%}.coupon.price,strong{font-weight:700}p{line-height:1.5em}body,html{height:100%;min-height:100%}section{padding:2rem 0}.btn{padding:1rem 2rem}.coupon{border:3px dashed#444!important;padding:2rem;color:#444!important}.coupon.price{font-size:3rem}@media only screen and(min-width:540px){.col-xs-centered{float:none;margin:auto}}@media only screen and(min-width:768px){section{padding:3rem 0}}@media only screen and(min-width:992px){section{padding:4rem 0}}@media only screen and(min-width:1200px){.coupon.disclaimer{font-size:1rem}}.margin-bottom-2x{margin-bottom:2rem}@media only screen and(max-width:539px){.col-xxs-12{padding:0 15px;width:100%}}.btn-main-outline{background-color:transparent;border-color:#444!important;color:#444!important}.btn-main-outline:hover,.btn-main-outline:focus,.btn-main-outline:active{background-color:#444!important;color:#fff}.coupon.disclaimer{font-size:1rem;border-color:#444!important;color:#333}</style><script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.js"></script></head><body><section><div class="container"><div class="row"><div class="col-xxs-12 col-xs-8 col-xs-centered col-sm-6 col-md-4 col-lg-4 margin-bottom-2x">`).append(content).append(`</div></div></section><script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script><script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script></body></html>`);
    window.frames['printIframe'].focus();
    window.frames['printIframe'].print();
    setTimeout(() => {
        $(".printFrame").remove();
    }, 1);
    serviceData.event.push(
        printEvent = {
            'eventInfo': {
                'eventAction': 'click',
                'eventName': 'PrintCoupon'
            }
        });
};
$('button').on('click', printCoupon);

// function downloadCoupon() {
//     html2canvas(document.getElementById("coupon")).then(function (canvas) {
//         document.getElementById("generatedCanvas").appendChild(canvas);
//         document.getElementsByTagName("canvas")[0];
//         canvas.toDataURL("image/png");
//         var link = document.createElement('a');
//         link.href = canvas.toDataURL("image/png");
//         link.download = 'Coupon.png';
//         document.body.appendChild(link);
//         link.click();
//     });
// }

// Popup 3rd party site in new tab and size it like a modal

function popupModal(url, title, w, h) {
    var dualScreenLeft = window.screenLeft !== undefined ? window.screenLeft : screen.left;
    var dualScreenTop = window.screenTop !== undefined ? window.screenTop : screen.top;
    var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
    var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;
    var left = ((width / 2) - (w / 2)) + dualScreenLeft;
    var top = ((height / 2) - (h / 2)) + dualScreenTop;
    var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
    serviceData.event.push(
        popupModalEvent = {
            'eventInfo': {
                'eventAction': 'click',
                'eventName': 'Open New Window Modal'
            }
        });

    if (window.focus) {
        newWindow.focus();
    }
}

// Scroll to Top

$(window).scroll(function () {
    if ($(this).scrollTop() > 100) {
        $('#scroll').fadeIn();
    } else {
        $('#scroll').fadeOut();
    }
});
$('#scroll').click(function () {
    $("html, body").animate({scrollTop: 0}, 600);
    return false;
});


// Expire at end of Month
var serviceCurrentDate = new Date();
var serviceMonth = serviceCurrentDate.getUTCMonth() + 1;
var serviceDay = serviceCurrentDate.getUTCDate();
var serviceYear = serviceCurrentDate.getUTCFullYear();
var calculateLastDay = new Date(serviceCurrentDate.getFullYear(), serviceCurrentDate.getMonth() + 1, 0);
var serviceLastDay = calculateLastDay.getDate();
var expireDate = new Date(serviceCurrentDate.getFullYear(), serviceCurrentDate.getMonth() + 1, 0);
$('.expireDate').text('Expires ' + serviceMonth + '/' + serviceLastDay + '/' + serviceYear);


// TCPA Disclaimer
$( ".gform_footer.top_label, .x-time" ).append( "<hr><p><small>By using this service you accept the terms of our</span><a data-toggle='modal' data-target='#visitoragreement'> Visitor Agreement.</a></small></p><div id='visitoragreement' class='modal fade' role='dialog'><div class='modal-dialog'><div class='modal-content'><div class='modal-header'> <button type='button' class='close' data-dismiss='modal'>&times;</button><h4 class='modal-title'>Visitor Agreement</h4></div><div class='modal-body'><p>By clicking here, you authorize Naples Nissan, and its affiliates, to contact you by texts/calls which may include marketing and be by autodialer or pre-recorded messages. Consent is not required to purchase goods or services.</p></div><div class='modal-footer'> <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button></div></div></div></div>" );


