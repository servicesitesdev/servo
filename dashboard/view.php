<?php

/* ----------------------------------------------------------------------------- */
/* Field Callbacks */
/* ----------------------------------------------------------------------------- */

function dealer_name_callback($args)
{
    ?>
    <input type="text" id="dealer_name" class="dealer_name" name="dealer_name"
           value="<?php echo get_option('dealer_name') ?>">
    <p class="description dealer_name"> <?php echo $args[0] ?> </p>

    <?php
}

function dealer_tel_callback($args)
{
    ?>
    <input type="text" id="dealer_tel" class="dealer_tel" name="dealer_tel"
           value="<?php echo get_option('dealer_tel') ?>">
    <p class="description dealer_tel"> <?php echo $args[0] ?> </p>
    <?php
}

function dealer_tellink_callback($args)
{
    ?>
    <input type="text" id="dealer_tellink" class="dealer_tellink" name="dealer_tellink"
           value="<?php echo get_option('dealer_tellink') ?>">
    <p class="description dealer_tellink"> <?php echo $args[0] ?> </p>
    <?php
}

function dealer_location1_callback($args)
{
    ?>
    <input type="text" id="dealer_location1" class="dealer_location1" name="dealer_location1"
           value="<?php echo get_option('dealer_location1') ?>">
    <p class="description dealer_location1"> <?php echo $args[0] ?> </p>
    <?php
}

function dealer_location2_callback($args)
{
    ?>
    <input type="text" id="dealer_location2" class="dealer_location2" name="dealer_location2"
           value="<?php echo get_option('dealer_location2') ?>">
    <p class="description dealer_location2"> <?php echo $args[0] ?> </p>
    <?php
}


function lat_option_callback($args)
{
    ?>
    <input type="text" id="lat_option" class="lat_option" name="lat_option"
           value="<?php echo get_option('lat_option') ?>">
    <p class="description lat_option"> <?php echo $args[0] ?> </p>
    <?php
}

function lng_option_callback($args)
{
    ?>
    <input type="text" id="lng_option" class="lng_option" name="lng_option"
           value="<?php echo get_option('lng_option') ?>">
    <p class="description lng_option"> <?php echo $args[0] ?> </p>
    <?php
}

function dealer_servicehours_callback($args)
{
    ?>
    <textarea id="dealer_servicehours" class="dealer_servicehours" name="dealer_servicehours" rows="5"
              cols="50"><?php echo get_option('dealer_servicehours') ?></textarea>
    <p class="description dealer_servicehours"> <?php echo $args[0] ?> </p>
    <?php
}


function dealer_partshours_callback($args)
{
    ?>
 <textarea id="dealer_partshours" class="dealer_partshours" name="dealer_partshours" rows="5"
              cols="50"><?php echo get_option('dealer_partshours') ?></textarea>
    <p class="description dealer_partshours"> <?php echo $args[0] ?> </p>
    <?php
}


function dealer_mainsite_callback($args)
{
    ?>
    <input type="text" id="dealer_mainsite" class="dealer_mainsite" name="dealer_mainsite"
           value="<?php echo get_option('dealer_mainsite') ?>">
    <p class="description dealer_mainsite"> <?php echo $args[0] ?> </p>
    <?php
}

function dealer_locationlink_callback($args)
{
    ?>
    <input type="text" id="dealer_locationlink" class="dealer_locationlink" name="dealer_locationlink"
           value="<?php echo get_option('dealer_locationlink') ?>">
    <p class="description dealer_locationlink"> <?php echo $args[0] ?> </p>
    <?php
}

function dealer_gmblink_callback($args)
{
    ?>
    <input type="text" id="dealer_gmblink" class="dealer_gmblink" name="dealer_gmblink"
           value="<?php echo get_option('dealer_gmblink') ?>">
    <p class="description dealer_gmblink"> <?php echo $args[0] ?> </p>
    <?php
}

function dealer_partslink_callback($args)
{
    ?>
    <input type="text" id="dealer_partslink" class="dealer_partslink" name="dealer_partslink"
           value="<?php echo get_option('dealer_partslink') ?>">
    <p class="description dealer_partslink"> <?php echo $args[0] ?> </p>
    <?php
}

function dealer_accessories_callback($args)
{
    ?>
    <input type="text" id="dealer_accessories" class="dealer_accessories" name="dealer_accessories"
           value="<?php echo get_option('dealer_accessories') ?>">
    <p class="description dealer_accessories"> <?php echo $args[0] ?> </p>
    <?php
}

function dealer_tireslink_callback($args)
{
    ?>
    <input type="text" id="dealer_tireslink" class="dealer_tireslink" name="dealer_tireslink"
           value="<?php echo get_option('dealer_tireslink') ?>">
    <p class="description dealer_tireslink"> <?php echo $args[0] ?> </p>
    <?php
}

/* ----------------------------------------------------------------------------- */
/* Coupon Section */
/* ----------------------------------------------------------------------------- */

function coupon_expiredate_callback($args)
{
    ?>
    <input type="text" id="coupon_expiredate" class="coupon_expiredate" name="coupon_expiredate"
           value="<?php echo get_option('coupon_expiredate') ?>">
    <p class="description coupon_expiredate"> <?php echo $args[0] ?> </p>
    <?php
}

/* ----------------------------------------------------------------------------- */
/* Analytics Section */
/* ----------------------------------------------------------------------------- */

function gtm_id_callback($args)
{
    ?>
    <input type="text" id="gtm_id" class="gtm_id" name="gtm_id"
           value="<?php echo get_option('gtm_id') ?>">
    <p class="description gtm_id"> <?php echo $args[0] ?> </p>
    <?php
}

function meta_google_callback($args)
{
    ?>
    <input type="text" id="meta_google" class="meta_google" name="meta_google"
           value="<?php echo get_option('meta_google') ?>">
    <p class="description meta_google"> <?php echo $args[0] ?> </p>
    <?php
}

function boldchat_btn_callback($args)
{
    ?>
    <textarea type="text" id="boldchat_btn" class="boldchat_btn" name="boldchat_btn"
          rows="5" cols="50"><?php echo get_option('boldchat_btn') ?></textarea>
    <p class="description boldchat_btn"> <?php echo $args[0] ?> </p>
    <?php
}

function vwo_accountid_callback($args)
{
    ?>
    <input type="text" id="vwo_accountid" class="vwo_accountid" name="vwo_accountid"
           value="<?php echo get_option('vwo_accountid') ?>">
    <p class="description vwo_accountid"> <?php echo $args[0] ?> </p>
    <?php
}

function vwo_settingstolerance_callback($args)
{
    ?>
    <input type="text" id="vwo_settingstolerance" class="vwo_settingstolerance" name="vwo_settingstolerance"
           value="<?php echo get_option('vwo_settingstolerance') ?>">
    <p class="description vwo_settingstolerance"> <?php echo $args[0] ?> </p>
    <?php
}

function vwo_librarytolerance_callback($args)
{
    ?>
    <input type="text" id="vwo_librarytolerance" class="vwo_librarytolerance" name="vwo_librarytolerance"
           value="<?php echo get_option('vwo_librarytolerance') ?>">
    <p class="description vwo_librarytolerance"> <?php echo $args[0] ?> </p>
    <?php
}

function test_callback($args)
{
    ?>
    <textarea id="test" class="test" name="test" rows="5"
              cols="50"><?php echo get_option('test') ?></textarea>
    <p class="description gtmid"> <?php echo $args[0] ?> </p>
    <?php
}

function customcode_header_callback($args)
{
    ?>
    <textarea type="text" id="customcode_header" class="customcode_header" name="customcode_header"
          rows="5" cols="50"><?php echo get_option('customcode_header') ?></textarea>
    <p class="description customcode_header"> <?php echo $args[0] ?> </p>
    <?php
}

function customcode_footer_callback($args)
{
    ?>
    <textarea type="text" id="customcode_footer" class="customcode_footer" name="customcode_footer"
          rows="5" cols="50"><?php echo get_option('customcode_footer') ?></textarea>
    <p class="description customcode_footer"> <?php echo $args[0] ?> </p>
    <?php
}

