<?php
/**
 * Servo Theme Option Page
 */

function servo_theme_menu()
{
    add_menu_page('Servo Dashboard Settings', 'Servo Dashboard', 'administrator', 'servo_theme_options.php', 'servo_theme_page', get_template_directory_uri() . '/assets/servo-icon.png');
}

add_action('admin_menu', 'servo_theme_menu');

function servo_theme_page()
{
    //$servo_dashboard_url = $_SERVER['SERVER_NAME']. $_SERVER['REQUEST_URI'];
    if (isset($_GET['tab'])) {
        $active_tab = $_GET['tab'];
    } else {
        $active_tab = 'general';
    }
    ?>
    <div class="wrap">
        <?php settings_errors(); ?>

        <h2 class="nav-tab-wrapper">
            <a href="?page=servo_theme_options.php&tab=general"
               class="nav-tab <?php echo $active_tab == 'general' ? 'nav-tab-active' : ''; ?>">General</a>

            <a href="?page=servo_theme_options.php&tab=coupon"
               class="nav-tab <?php echo $active_tab == 'coupon' ? 'nav-tab-active' : ''; ?>">Coupon</a>

            <a href="?page=servo_theme_options.php&tab=customcode"
               class="nav-tab <?php echo $active_tab == 'customcode' ? 'nav-tab-active' : ''; ?>">Custom Code</a>

            <a href="?page=servo_theme_options.php&tab=analytics"
               class="nav-tab <?php echo $active_tab == 'analytics' ? 'nav-tab-active' : ''; ?>">Analytics</a>
        </h2>

        <form method="post" enctype="multipart/form-data" action="options.php">
            <?php
            wp_nonce_field('update-options');
            if ($active_tab == 'general') {
                settings_fields('general-settings-group');
                do_settings_sections('dealer-info'); ?>

                <h2>Shortcode Reference Sheet</h2>
                <table class="table table-striped table-bordered">
                    <tbody>
                    <tr>
                        <th>Variable</th>
                        <th>Description</th>
                    </tr>
                    <tr>
                        <td><b>[dealer-name]</b></td>
                        <td><b>The dealership name</b></td>
                    </tr>
                    <tr>
                        <td><b>[mainsite-link]</b></td>
                        <td><b>Displays the main dealership website link formatted for html.</b></td>
                    </tr>
                    <tr>
                        <td><b>[service-tel]</b></td>
                        <td><b>Displays telephone number formatted for website.</b></td>
                    </tr>
                    <tr>
                        <td><b>[tel-link]</b></td>
                        <td><b>Displays telephone number for "tel:" anchor tags.</b></td>
                    </tr>
                    <tr>
                        <td><b>[location-link]</b></td>
                        <td><b>Displays Google Map anchor link formatted for anchor tags.</b></td>
                    </tr>
                    <tr>
                        <td><b>[location-blink]</b></td>
                        <td><b>Displays Google My Business Map anchor link formatted for anchor tags.</b></td>
                    </tr>
                    <tr>
                        <td><b>[location-1]</b></td>
                        <td><b>Street Address</b></td>
                    </tr>
                    <tr>
                        <td><b>[location-2]</b></td>
                        <td><b>City, State, Zip</b></td>
                    </tr>
                    <tr>
                        <td><b>[lat-option]</b></td>
                        <td><b>Displays latitude formatted for google map script.</b></td>
                    </tr>

                    <tr>
                        <td><b>[lng-option]</b></td>
                        <td><b>Displays longitude formatted for google map script.</b></td>
                    </tr>
                    <tr>
                        <td><b>[service-hours]</b></td>
                        <td><b>Displays service hours formatted for website.</b></td>
                    </tr>
                    <tr>
                        <td><b>[parts-hours]</b></td>
                        <td><b>Displays parts hours formatted for website.</b></td>
                    </tr>
                    <tr>
                        <td><b>[parts-link]</b></td>
                        <td><b>Displays parts link formatted for html.</b></td>
                    </tr>
                    <tr>
                        <td><b>[tires-link]</b></td>
                        <td><b>Displays order tires link formatted for html.</b></td>
                    </tr>
                    <tr>
                        <td><b>[coupon-expiredate]</b></td>
                        <td><b>Displays expiration date on service coupons.</b></td>
                    </tr>
                    </tbody>
                </table>

                <?php
            } else if ($active_tab == 'coupon') {
                settings_fields('coupon-settings-group');
                do_settings_sections('coupon'); ?>
                <?php

            } else if ($active_tab == 'customcode') {
                settings_fields('customcode-settings-group');
                do_settings_sections('customcode'); ?>

                <?php
            } else if ($active_tab == 'analytics') {
                settings_fields('analytics-settings-group');
                do_settings_sections('analytics'); ?>
                <?php
            }

            ?>
            <?php submit_button(); ?>
        </form>
    </div>
    <?php
}

/* ----------------------------------------------------------------------------- */
/* Setting Sections And Fields */
/* ----------------------------------------------------------------------------- */

function servo_register_settings()
{
    add_settings_section(
        'general_settings_section',
        'Dealer Info',
        'general_settings_section_callback',
        'dealer-info'

    );

        add_settings_section(
            'coupon_section',
            'Coupon Settings',
            'coupon_section_callback',
            'coupon'
        );

    add_settings_section(
        'customcode_section',
        'Custom Code Section',
        'customcode_section_callback',
        'customcode'
    );

    add_settings_section(
        'analytics_section',
        'Analytics',
        'analytics_section_callback',
        'analytics'
    );

    /* ----------------------------------------------------------------------------- */
    /* General Settings */
    /* ----------------------------------------------------------------------------- */

    add_settings_field(
        'dealer_name',
        'Dealer Name',
        'dealer_name_callback',
        'dealer-info',
        'general_settings_section',
        array(
            'Nicole Sereno Nissan',
        )
    );

    register_setting(
        'general-settings-group',
        'dealer_name'
    );

    add_settings_field(
        'dealer_tel',
        'Dealer Number',
        'dealer_tel_callback',
        'dealer-info',
        'general_settings_section',
        array(
            '0.000.000.0000',
        )
    );
    register_setting(
        'general-settings-group',
        'dealer_tel'
    );

    add_settings_field(
        'dealer_tellink',
        'Dealer Number Link',
        'dealer_tellink_callback',
        'dealer-info',
        'general_settings_section',
        array(
            'Displays telephone number for "tel:" anchor tags. <small><b>(do not include "tel:")</b></small>',
        )
    );
    register_setting(
        'general-settings-group',
        'dealer_tellink'
    );


    add_settings_field(
        'dealer_location1',
        'Dealer Location 1',
        'dealer_location1_callback',
        'dealer-info',
        'general_settings_section',
        array(
            'Street Address',
        )
    );
    register_setting(
        'general-settings-group',
        'dealer_location1'
    );

    add_settings_field(
        'dealer_location2',
        'Dealer Location 2',
        'dealer_location2_callback',
        'dealer-info',
        'general_settings_section',
        array(
            'City, State, Zip',
        )
    );
    register_setting(
        'general-settings-group',
        'dealer_location2'
    );

    /* ----------------------------------------------------------------------------- */
    /* Google Map Settings */
    /* ----------------------------------------------------------------------------- */

    add_settings_field(
        'lat_option',
        'Latitude:',
        'lat_option_callback',
        'dealer-info',
        'general_settings_section',
        array(
            '',
        )
    );
    register_setting(
        'general-settings-group',
        'lat_option'
    );

    add_settings_field(
        'lng_option',
        'Longitude:',
        'lng_option_callback',
        'dealer-info',
        'general_settings_section',
        array(
            '',
        )
    );
    register_setting(
        'general-settings-group',
        'lng_option'
    );

    /* ----------------------------------------------------------------------------- */
    /* HOURS */
    /* ----------------------------------------------------------------------------- */

    add_settings_field(
        'dealer_servicehours',
        'Dealer Service Hours',
        'dealer_servicehours_callback',
        'dealer-info',
        'general_settings_section',
        array(
            '&lt;li&gt;Monday - Friday: 0:00am to 0:00pm&lt;/li&gt;<br/>&lt;li&gt;Saturday: 0:00am to 0:00pm&lt;/li&gt;<br/>&lt;li&gt;Sunday: Closed&lt;/li&gt;',
        )
    );
    register_setting(
        'general-settings-group',
        'dealer_servicehours'
    );


    add_settings_field(
        'dealer_partshours',
        'Dealer Parts Hours',
        'dealer_partshours_callback',
        'dealer-info',
        'general_settings_section',
        array(
            '&lt;li&gt;Monday - Friday: 0:00am to 0:00pm&lt;/li&gt;<br/>&lt;li&gt;Saturday: 0:00am to 0:00pm&lt;/li&gt;<br/>&lt;li&gt;Sunday: Closed&lt;/li&gt;',
        )
    );
    register_setting(
        'general-settings-group',
        'dealer_partshours'
    );

    /* ----------------------------------------------------------------------------- */
    /* URL Links */
    /* ----------------------------------------------------------------------------- */

    add_settings_field(
        'dealer_mainsite',
        'MainSite Link',
        'dealer_mainsite_callback',
        'dealer-info',
        'general_settings_section',
        array(
            'www.example.com',
        )
    );

    register_setting(
        'general-settings-group',
        'dealer_mainsite'
    );


    add_settings_field(
        'dealer_locationlink',
        'Dealer Location Link',
        'dealer_locationlink_callback',
        'dealer-info',
        'general_settings_section',
        array(
            'www.example.com',
        )
    );
    register_setting(
        'general-settings-group',
        'dealer_locationlink'
    );
    add_settings_field(
        'dealer_gmblink',
        'Dealer Google My Business Link',
        'dealer_gmblink_callback',
        'dealer-info',
        'general_settings_section',
        array(
            'www.example.com',
        )
    );

    register_setting(
        'general-settings-group',
        'dealer_gmblink'
    );

    add_settings_field(
        'dealer_partslink',
        'Dealer Parts Link',
        'dealer_partslink_callback',
        'dealer-info',
        'general_settings_section',
        array(
            'www.example.com',
        )
    );
    register_setting(
        'general-settings-group',
        'dealer_partslink'
    );

    add_settings_field(
        'dealer_accessories',
        'Accessories Link',
        'dealer_accessories_callback',
        'dealer-info',
        'general_settings_section',
        array(
            'www.example.com',
        )
    );
    register_setting(
        'general-settings-group',
        'dealer_accessories'
    );

    add_settings_field(
        'dealer_tireslink',
        'Dealer Tires Link',
        'dealer_tireslink_callback',
        'dealer-info',
        'general_settings_section',
        array(
            'www.example.com',
        )
    );
    register_setting(
        'general-settings-group',
        'dealer_tireslink'
    );

    /* ----------------------------------------------------------------------------- */
    /* Coupon Settings */
    /* ----------------------------------------------------------------------------- */

    add_settings_field(
        'coupon_expiredate',
        'Coupon Expiration Date',
        'coupon_expiredate_callback',
        'coupon',
        'coupon_section',
        array(
            '12/04/2033',
        )
    );

    register_setting(
        'coupon-settings-group',
        'coupon_expiredate'
    );

    /* ----------------------------------------------------------------------------- */
    /* Custom Code Settings */
    /* ----------------------------------------------------------------------------- */

    add_settings_field(
        'customcode_header',
        'customcode Header',
        'customcode_header_callback',
        'customcode',
        'customcode_section',
        array(
            '',
        )
    );
    register_setting(
        'customcode-settings-group',
        'customcode_header'
    );

    add_settings_field(
        'customcode_footer',
        'customcode Footer',
        'customcode_footer_callback',
        'customcode',
        'customcode_section',
        array(
            '',
        )
    );
    register_setting(
        'customcode-settings-group',
        'customcode_footer'
    );

    /* ----------------------------------------------------------------------------- */
    /* Analytics Settings */
    /* ----------------------------------------------------------------------------- */

    add_settings_field(
        'gtm_id',
        'GTM ID:',
        'gtm_id_callback',
        'analytics',
        'analytics_section',
        array(
            'Place GTM Unique ID in textbox',
        )
    );
    register_setting(
        'analytics-settings-group',
        'gtm_id'
    );

    add_settings_field(
        'meta_google',
        'Google Search Console:',
        'meta_google_callback',
        'analytics',
        'analytics_section',
        array(
            '',
        )
    );
    register_setting(
        'analytics-settings-group',
        'meta_google'
    );

    add_settings_field(
        'boldchat_btn',
        'Bold Chat button:',
        'boldchat_btn_callback',
        'analytics',
        'analytics_section',
        array(
            'Place BoldChat button in header',
        )
    );
    register_setting(
        'analytics-settings-group',
        'boldchat_btn'
    );

}

add_action('admin_init', 'servo_register_settings');

function general_settings_section_callback()
{
    echo '<p>General Options</p>';
}

function coupon_section_callback()
{
//    echo '<p>Coupon Options</p>';
}

function customcode_section_callback()
{
    echo '<p>Custom Code Options</p>';
}

function analytics_section_callback()
{
    echo '<p>Third Party Integrations</p>';
}

require_once('view.php');